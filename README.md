# proxmox

## install proxmox hardware

Hardware is an Beelink U55-A-8256UD0W64PRO
https://smile.amazon.de/gp/product/B07K653VFM

1. [Download proxmox-ve_7.1-2.iso](https://www.proxmox.com/de/downloads/category/iso-images-pve)
2. Flash iso to USB Stick with [balenaEtcher](https://www.balena.io/etcher/)
3. setup beelink (network, mouse, keyboard, monitor)
4. plug in the usb stick
5. power on and hit F7 to enter boot menu and select the usb stick
6. install proxmox (all settings default)
7. https://pve.lan:8006 or https://192.168.1.186:8006
8. goto pve/shell<br>
   ```
   apt update
   apt -y dist-upgrade
   apt -y install unattended-upgrades
   apt clean

   pveam update
   pveam download local debian-11-standard_11.0-1_amd64.tar.gz

   nano /etc/network/interfaces
   ```
   ``` ini
   auto lo
   iface lo inet loopback
   
   iface enp1s0 inet manual
   
   auto vmbr0
   #iface vmbr0 inet static
           #address 192.168.1.186/24
           #gateway 192.168.1.1
   iface vmbr0 inet dhcp
           bridge-ports enp1s0
           bridge-stp off
           bridge-fd 0
   
   iface wlp2s0 inet manual
   ```
   ```
   reboot
   ```


## install mosquitto

1. https://pve.lan:8006
2. create CT 
    - Hostmame: mosquitto
    - Load SSH Key File
    - Template: debian-11-standard_11.0-1_amd64.tar.gz
    - IPv4: DHCP
    - [x] Start after created
3. Options: Start at boot: yes
4. install mosquitto
   ```
   ssh root@mosquitto.lan

   apt update
   apt -y dist-upgrade
   apt -y install unattended-upgrades
   apt -y install mosquitto
   apt clean

   nano /etc/mosquitto/conf.d/protocols.conf
   ```
   ``` ini
   listener 1883
   protocol mqtt
   
   listener 1884
   protocol websockets
   ```
   ```
   nano /etc/mosquitto/conf.d/default.conf
   ```
   ``` ini
   allow_anonymous true
   ```
   ```
   service mosquitto restart
   ```


## install zigbee2mqtt

1. plugin zigbee usb stick
2. https://pve.lan:8006
3. goto pve/shell<br>
   ```
   apt -y install ser2net

   nano /etc/ser2net.yaml
   ```
   ``` yaml
   # add following at end of file
   connection: &con01
       accepter: tcp,20108
       connector: serialdev,/dev/serial/by-id/usb-Silicon_Labs_CP2102N_USB_to_UART_Bridge_Controller_204e7f9a3893eb11a014194f3d98b6d1-if00-port0,115200n81,local
        options:
           kickolduser: true

   ```
   ```
   service ser2net reload

   nano /etc/systemd/system/ser2net.timer
   ```
   ``` ini
   [Unit]
   Description=Bootup delay for ser2net service
   
   [Timer]
   OnBootSec=2min
   Unit=ser2net.service
   
   [Install]
   WantedBy=multi-user.target
   ```
   ```
   chmod -R 0644 /etc/systemd/system/ser2net.timer
   systemctl enable ser2net.timer
   systemctl disable ser2net.service
   ```

4. create CT 
    - Hostmame: zigbee2mqtt
    - Load SSH Key File
    - Template: debian-11-standard_11.0-1_amd64.tar.gz
    - IPv4: DHCP
    - [x] Start after created
5. Options: Start at boot: yes
6. install zigbee2mqtt
   ```
   ssh root@zigbee2mqtt.lan

   apt update
   apt -y dist-upgrade
   apt -y install unattended-upgrades
   apt -y install nodejs npm git make g++ gcc
   apt clean

   git clone https://github.com/Koenkk/zigbee2mqtt.git /opt/zigbee2mqtt

   cd /opt/zigbee2mqtt

   npm ci
   npm audit fix

   nano /opt/zigbee2mqtt/data/configuration.yaml
   ```
   ``` yaml
   # MQTT settings
     server: 'mqtt://mosquitto.lan'

   # Serial settings
   serial:
     port: 'tcp://pve.lan:20108'

   frontend:
     port: 80

   advanced:
     network_key: GENERATE
   ```
   ```
   nano /etc/systemd/system/zigbee2mqtt.service
   ```
   ``` ini
   [Unit]
   Description=zigbee2mqtt
   After=network.target

   [Service]
   ExecStart=/usr/bin/npm start
   WorkingDirectory=/opt/zigbee2mqtt
   StandardOutput=inherit
   # Or use StandardOutput=null if you don't want Zigbee2MQTT messages filling syslog, for more options see systemd.exec(5)
   StandardError=inherit
   Restart=always
   #User=pi

   [Install]
   WantedBy=multi-user.target
   ```
   ```
   systemctl enable zigbee2mqtt.service
   systemctl start zigbee2mqtt.service
   ```


